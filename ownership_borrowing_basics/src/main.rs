fn main() {
    println!("\n\nHello! \n\
    This is just a play project, to get familiar with ownership and borrowing in Rust.\n\n\
    There are many examples in the code in \"main.rs\" file, so please don't be lazy\n\
    and explore it.\n\
    Enjoy!!!");

    simple_ownership();
    borrowing_examples();
}

// function to demonstrate basic ownership rules
// * Each value in Rust has a variable that’s called its owner.
// * There can only be one owner at a time.
// * When the owner goes out of scope, the value will be dropped.
fn simple_ownership() {
    println!("\n--------- Simple Ownership Examples ---------");
    println!("\nOn re-assign, ownership of types, which implement \"Clone\" trait is not moved.\n\
             Instead copy of the object is created.\nIf you want to read about \"Copy\" trait, \
             including list of types which implement it, please visit this link:\n\
             https://doc.rust-lang.org/std/marker/trait.Copy.html");

    // ------------------------
    println!("\n\nPlease consider next code snippet:\n\
    \tlet number = 42;\n\
    \tprintln!(\"number is {{}}\", number);\n\
    \tlet another_number = number; // !!! <- value is copied, ownership is NOT moved\n\
    \t// here I still can use both \"number\" and \"another_number\" variables\n\
    \tprintln!(\"number is {{}}, another number is {{}}\", number, another_number);\n\n\
    Output will be:\n");

    let number = 42;
    println!("number is {}", number);
    let another_number = number; // !!! <- value is copied, ownership is NOT moved
    // here I still can use both "number" and "another_number" variables
    println!("number is {}, another number is {}", number, another_number);

    // ------------------------
    println!("\nBut things are different for types, which do not implement \"Copy\" trait, f.e. \"String\".\n\
    Type \"String\" keeps its data on heap, and can grow pretty big, so it is not efficient to copy data on each\n\
    assign operation.\n\
    Please consider the next code snippet:\n\
    \tlet string = String::from(\"hello\");\n\
    \tprintln!(\"string is \\\"{{}}\\\"\", string)\n\
    \tlet another_string = string;  // !!! <- moving ownership to \"another_string\" - we cannot use \"string\" any more!!!\n\
    \tprintln!(\"string is \\\"{{}}\\\" another string is \\\"{{}}\\\"\", string, another_string); \n\n\
    If we tried to compile it, we would get an error:\n\n
error[E0382]: borrow of moved value: `string`
  --> src/main.rs:67:59
   |
63 |     let string = String::from(\"hello\");
   |         ------ move occurs because `string` has type `String`, which does not implement the `Copy` trait
65 |     let another_string = string;  // !!! <- moving ownership to \"another_string\" - we cannot use \"string\" any more!!!
   |                          ------ value moved here
66 |     // if uncomment line below, we will get compilation error, because \"another_string\" owns value now, and \"string\" owns nothing
67 |     println!(\"string is \\\"{{}}\\\" another string is \\\"{{}}\\\"\", string, another_string);
   |                                                           ^^^^^^ value borrowed here after move\n\n\
   Let's comment out that problematic line, so we can compile our program, and use this one instead:\n\
   \tprintln!(\"another string is \\\"{{}}\\\"\", another_string);\n\
   Output will be:\n");

    let string = String::from("hello");
    println!("string is \"{}\"", string);
    let another_string = string;  // !!! <- moving ownership to "another_string" - we cannot use "string" any more!!!
    println!("another string is \"{}\"", another_string);
    // if uncomment line below, we will get compilation error, because "another_string" owns value now, and "string" owns nothing
    // println!("string is \"{}\" another string is \"{}\"", string, another_string);
}

// function to demonstrate borrowing rules in the Rust
// * There can be many immutable references or single mutable one
fn borrowing_examples() {
    println!("\n--------- Simple Borrowing Examples ---------");

    println!("\nStatic strings in fact is a reference to the memory (&str):\n\
    \tlet static_string = \"static\";  // !!! <- here static_string is of type &str\n\
    \tprintln!(\"static_string is \\\"{{}}\\\"\", static_string);\n\
    \tlet another_static_string = static_string; // !!! <- another immutable reference\n\
    \t// so I still can use both \"static_string\" and \"another_static_string\" variables\n\
    \tprintln!(\"static_string is \\\"{{}}\\\", another static_string is \\\"{{}}\\\"\", static_string, another_static_string);\n\n\
    Output will be:\n");

    let static_string = "static";
    println!("static_string is \"{}\"", static_string);
    let another_static_string = static_string; // !!! <- here static_string is of type &str
    // so I still can use both "static_string" and "another_static_string" variables
    println!("static_string is \"{}\", another static_string is \"{}\"", static_string, another_static_string);
}

